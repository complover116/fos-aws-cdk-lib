package com.factoryofshit.fos_aws_cdk_lib.permanent_infra

import com.factoryofshit.fos_aws_cdk_lib.FOSEnvironment
import software.amazon.awscdk.Stack
import software.amazon.awscdk.StackProps
import software.amazon.awscdk.Tags
import software.amazon.awscdk.services.autoscaling.AutoScalingGroup
import software.amazon.awscdk.services.ec2.*
import software.amazon.awscdk.services.ecs.*
import software.amazon.awscdk.services.elasticloadbalancingv2.ApplicationLoadBalancer
import software.amazon.awscdk.services.iam.PolicyStatement
import software.amazon.awscdk.services.s3.Bucket
import software.constructs.Construct

class PermanentInfraStack(scope: Construct, id: String, props: StackProps, fosEnvironment: FOSEnvironment) : Stack(scope, id, props) {
    init {
//        val vpc = Vpc.Builder.create(this, "FOSVpc")
//            .createInternetGateway(true)
//            .availabilityZones(listOf("eu-north-1a"))
//            .natGateways(0)
//            .ipProtocol(IpProtocol.DUAL_STACK)
//            .vpcName("FOSVpc-${fosEnvironment.name}")
//            .build()

        // TODO: Replace with my custom VPC
        val defaultVpc = Vpc.fromLookup(this, "defaultVPC", VpcLookupOptions.builder().isDefault(true).build())

        val vpc = defaultVpc

        Tags.of(this).add("FOSPermanentInfraEnvironment", fosEnvironment.name)

        ApplicationLoadBalancer.Builder.create(this, "FOSLoadBalancer")

        val cluster = Cluster.Builder.create(this, "FOSCluster")
            .clusterName("FOSCluster-"+fosEnvironment.name)
            .vpc(vpc)
            .build()


        val autoScalingGroup = AutoScalingGroup.Builder.create(this, "FOSAutoScalingGroup")
            .instanceType(InstanceType.of(InstanceClass.T3, InstanceSize.NANO))
            .machineImage(EcsOptimizedImage.amazonLinux2(AmiHardwareType.STANDARD))
            .newInstancesProtectedFromScaleIn(false)
            .vpcSubnets(SubnetSelection.builder().subnetType(SubnetType.PUBLIC).build())
            .vpc(vpc)
            .maxCapacity(2)
            .minCapacity(1)
            .build()


        val capacityProvider = AsgCapacityProvider.Builder.create(this, "FOSCapacityProvider")
            .autoScalingGroup(autoScalingGroup)
            .enableManagedTerminationProtection(false)
            .build()

//        cluster.connections.addSecurityGroup()

        cluster.addAsgCapacityProvider(capacityProvider)
    }
}
