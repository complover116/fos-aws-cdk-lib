package com.factoryofshit.fos_aws_cdk_lib

import software.amazon.awscdk.services.ec2.*
import software.amazon.awscdk.services.ecs.Cluster
import software.amazon.awscdk.services.ecs.ClusterAttributes
import software.amazon.awscdk.services.ecs.ICluster
import software.amazon.awscdk.services.elasticloadbalancingv2.*
import software.amazon.awscdk.services.rds.DatabaseInstance
import software.amazon.awscdk.services.rds.DatabaseInstanceAttributes
import software.amazon.awscdk.services.rds.IDatabaseInstance
import software.amazon.awscdk.services.route53.HostedZone
import software.amazon.awscdk.services.route53.HostedZoneProviderProps
import software.amazon.awscdk.services.route53.IHostedZone
import software.amazon.awscdk.services.secretsmanager.ISecret
import software.amazon.awscdk.services.secretsmanager.Secret
import software.constructs.Construct

enum class FOSEnvironment {
    DEV,
    PROD
}

class FOSPermanentInfra(scope: Construct, id: String, fosEnvironment: FOSEnvironment) : FOSConstruct(scope, id, fosEnvironment) {
    val vpc : IVpc by lazy {
        Vpc.fromLookup(
            this, "FOSVpc", VpcLookupOptions.builder()
                .isDefault(true)
//                .tags(mapOf(Pair("FOSPermanentInfraEnvironment", fosEnvironment.name)))
                .build()
        )
    }



    val cluster : ICluster by lazy {
        Cluster.fromClusterAttributes(
            this, "FOSCluster",
            ClusterAttributes.builder()
                .clusterName("FOSCluster-"+fosEnvironment.name)
                .vpc(vpc)
                .build()
        )
    }

    val loadBalancer : IApplicationLoadBalancer by lazy {
        ApplicationLoadBalancer.fromLookup(
            this, "FOSLoadBalancer",
            ApplicationLoadBalancerLookupOptions.builder()
                .loadBalancerTags(mapOf(Pair("FOSPermanentInfraEnvironment", fosEnvironment.name)))
                .build()
        )
    }

    val httpListener : IApplicationListener by lazy {
        ApplicationListener.fromLookup(this, "FOSHTTPListener", ApplicationListenerLookupOptions.builder()
            .listenerProtocol(ApplicationProtocol.HTTP)
            .loadBalancerArn(loadBalancer.loadBalancerArn)
            .listenerPort(80)
            .build())
    }

    val hostedZone : IHostedZone by lazy {
        HostedZone.fromLookup(this, "HostedZone", HostedZoneProviderProps.builder()
            .domainName("factoryofshit.com")
            .build())
    }
}
