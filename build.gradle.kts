plugins {
    id("java")
    id("maven-publish")
    application
    kotlin("jvm")
}

group = "com.factoryofshit"
version = "1.0"

repositories {
    mavenCentral()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = group.toString()
            artifactId = "fos-aws-cdk-lib"
            version = "1.0-SNAPSHOT"

            from(components["java"])
        }
    }
}

application {
    mainClass = "com.factoryofshit.fos_aws_cdk_lib.FOSCDKAppKt"
}

dependencies {
    // https://mvnrepository.com/artifact/software.amazon.awscdk/aws-cdk-lib
    implementation("software.amazon.awscdk:aws-cdk-lib:2.176.0")
    // https://mvnrepository.com/artifact/software.constructs/constructs
    implementation("software.constructs:constructs:10.4.2")
//    implementation(kotlin("stdlib-jdk8"))
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(17)
}
