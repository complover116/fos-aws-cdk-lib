package com.factoryofshit.fos_aws_cdk_lib.app_stacks

import com.factoryofshit.fos_aws_cdk_lib.FOSContainerService
import com.factoryofshit.fos_aws_cdk_lib.FOSEnvironment
import software.amazon.awscdk.Stack
import software.amazon.awscdk.StackProps
import software.amazon.awscdk.services.ecs.ContainerImage
import software.amazon.awscdk.services.ecs.RepositoryImageProps
import software.amazon.awscdk.services.secretsmanager.Secret
import software.constructs.Construct

class Kadastra(scope: Construct, id: String, props: StackProps, fosEnvironment: FOSEnvironment) : Stack(scope, id, props) {
    init {

        val telegramApiKey = Secret.fromSecretNameV2(
            this,
            "TelegramApiKey",
            "Kadastra/TelegramAPIKey-"+fosEnvironment.name
        ).secretValue.unsafeUnwrap()

        val imageTag = when (fosEnvironment) {
            FOSEnvironment.DEV -> "latest"
            FOSEnvironment.PROD -> "latest"
        }

        FOSContainerService(
            scope=this,
            id="kadastra",
            fosEnvironment=fosEnvironment,
            containerEnvVars = mapOf("API_KEY" to telegramApiKey),
            image=ContainerImage.fromRegistry("registry.gitlab.com/complover116/kadastra:$imageTag", RepositoryImageProps.builder()
                .credentials(Secret.fromSecretNameV2(this, "GitlabSecret", "GitlabSecret"))
                .build())
        )
    }
}
