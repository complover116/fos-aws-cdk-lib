package com.factoryofshit.fos_aws_cdk_lib

import software.constructs.Construct

open class FOSConstruct(scope: Construct, id: String, fosEnvironment: FOSEnvironment) : Construct(scope, id) {}
