package com.factoryofshit.fos_aws_cdk_lib

import software.amazon.awscdk.services.ecs.*
import software.amazon.awscdk.services.elasticloadbalancingv2.*
import software.amazon.awscdk.services.logs.RetentionDays
import software.amazon.awscdk.services.route53.CnameRecord
import software.constructs.Construct

class FOSContainerService constructor(
    scope: Construct,
    id: String,
    fosEnvironment: FOSEnvironment,
    image: ContainerImage,
    memoryLimitMiB: Int = 100,
    memoryReservationMiB: Int = 100,
    cpu: Int = 100,
    httpSubdomain: String? = null,
    containerEnvVars: Map<String, String> = emptyMap(),
) : FOSConstruct(scope, id, fosEnvironment) {

//    class Builder(val scope: Construct, val id: String, val image: ContainerImage) {
//        internal var memoryLimitMiB = 100
//        internal var memoryReservationMiB = 100
//        internal var cpu = 100
//        internal var httpSubdomain: String? = null
//
//        fun registerHTTPSubdomain(value: String): Builder {
//            httpSubdomain = value
//            return this
//        }
//
//        fun build(): FOSContainerService {
//            return FOSContainerService(this)
//        }
//    }
    init {
        val taskDefinition = Ec2TaskDefinition.Builder.create(this, "TaskDefinition")
            .build()

        taskDefinition.addContainer(
            "Container", ContainerDefinitionOptions.builder()
                .image(image)
                .memoryLimitMiB(memoryLimitMiB)
                .memoryReservationMiB(memoryReservationMiB)
                .cpu(cpu)
                .environment(containerEnvVars)
                .portMappings(listOf(PortMapping.builder().containerPort(80).build()))
                .logging(LogDriver.awsLogs(AwsLogDriverProps.builder()
                    .streamPrefix("FOSContainerService")
                    .logRetention(RetentionDays.ONE_WEEK)
                    .build()))
                .build()
        )

        val permanentInfra = FOSPermanentInfra(this, "PermanentInfra", fosEnvironment)

        val serviceBuilder = Ec2Service.Builder.create(this, "Service")
            .cluster(permanentInfra.cluster)
            .taskDefinition(taskDefinition)
            .circuitBreaker(DeploymentCircuitBreaker.builder().enable(true).build())
            .desiredCount(1)

        if (httpSubdomain != null) {
            CnameRecord.Builder.create(this, "CNameRecord")
                .zone(permanentInfra.hostedZone)
                .recordName(httpSubdomain)
                .domainName(permanentInfra.loadBalancer.loadBalancerDnsName)
                .build()

            val targetGroup = ApplicationTargetGroup.Builder.create(this, "TargetGroup")
                .vpc(permanentInfra.vpc)
                .protocol(ApplicationProtocol.HTTP)
                .build()

            permanentInfra.httpListener.addTargetGroups(httpSubdomain, AddApplicationTargetGroupsProps.builder()
                .targetGroups(listOf(targetGroup))
                .conditions(listOf(ListenerCondition.hostHeaders(listOf("${httpSubdomain}.factoryofshit.com"))))
                .priority(1)
                .build())

            targetGroup.addTarget(serviceBuilder.build())
        } else {
            serviceBuilder.build()
        }
    }
}
