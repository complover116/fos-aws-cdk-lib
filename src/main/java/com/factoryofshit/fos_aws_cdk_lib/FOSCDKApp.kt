package com.factoryofshit.fos_aws_cdk_lib

import com.factoryofshit.fos_aws_cdk_lib.app_stacks.Kadastra
import com.factoryofshit.fos_aws_cdk_lib.permanent_infra.PermanentInfraStack
import software.amazon.awscdk.App
import software.amazon.awscdk.Environment
import software.amazon.awscdk.StackProps

fun main() {
    val awsEnvironment = Environment.builder()
        .account(System.getenv("CDK_DEFAULT_ACCOUNT"))
        .region(System.getenv("CDK_DEFAULT_REGION"))
        .build()

    val environment = if(System.getenv("FOS_ENV") == "prod") FOSEnvironment.PROD else FOSEnvironment.DEV

    val app = App()

    PermanentInfraStack(app, "FOSPermanentInfraStack-${environment.name}", StackProps.builder()
        .env(awsEnvironment)
        .description("Permanent shared infrastructure for deploying FOS apps")
        .terminationProtection(false)
        .build(),
        environment
    )

    Kadastra(app, "KadastraStack-${environment.name}", StackProps.builder()
        .env(awsEnvironment)
        .description("A Telegram RPG")
        .terminationProtection(false)
        .build(),
        environment
    )
    app.synth()
}
